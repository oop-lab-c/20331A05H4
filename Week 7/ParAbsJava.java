import java.util.*;
abstract class Boss{
    void deadline(){ 
        System.out.println("Time Limit : 10 Weeks");
    }
    abstract void projectchoice(); 
}

class employee extends Boss{
    void projectchoice(){ 
        System.out.println("Project: Online examination system ");
    }
}
public class ParAbsJava {
    public static void main(String[] args){
        employee obj = new employee();
        obj.projectchoice();
        obj.deadline();
    }
}
