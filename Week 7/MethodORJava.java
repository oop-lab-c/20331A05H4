import java.util.*;
class parent{
    void age(int num){   
        System.out.println("Parent age "+ num);
    }
}

class child extends parent{
    void age(int num){  
        System.out.println("Child age "+ num);
    }
}

public class MethodORJava {
   public static void main(String [] args){
        child obj = new child();
        obj.age(24); 
   } 
}

