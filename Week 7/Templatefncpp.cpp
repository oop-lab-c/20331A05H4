#include <iostream>
using namespace std;

template <class T>
class Number {        
    T num;
   public:
    Number(T n){
       num = n;
    }  
    T getNum() {
        return num;
    }
};

template <typename S>        
S add(S a, S b){
    return a+b;
}
int main() {

    Number<char> Obj(44);
    Number<int> Obj2(44);
    cout << "char Number = " << Obj.getNum() << endl; 
    cout << "int Number = " << Obj2.getNum() << endl;    
    cout << "float addition = " << add < float > (1.6 , 6.4) << endl;      
    cout << "int addition = " << add < int > (4 , 7) << endl;      
    return 0;
}
