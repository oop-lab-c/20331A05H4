#include<iostream>
using namespace std;
 class opoverload{
     float real,imaginary;
     public:
     opoverload(){
            real=0;
            imaginary=0;
     }
     void insert(){
         cout<<"Enter real and imaginary parts of complex number"<<endl;
         cin>>real>>imaginary;
     }
     opoverload operator + (const opoverload &obj){         //Overloading constructor
         opoverload temp;
         temp.real = real + obj.real;
         temp.imaginary = imaginary + obj.imaginary;
         return temp;
     }
     void print(){
         if(imaginary>0)
            cout<<"Output "<<real<<"+"<<imaginary<<"i"<<endl;
        else    
            cout<<"Output "<<real<<imaginary<<"i"<<endl;
     }
 };
 int main(){
     opoverload obj1 , obj2 , obj3;
     cout<<"Enter two complex numbers "<<endl;
     obj1.insert();
     obj2.insert();
     obj3 = obj1 + obj2;
     obj3.print();
 }
