#include<iostream>
using namespace std;

class parent{
         virtual void profession()=0;
};
class son: public parent{
    public:
        void profession(){     
            cout<<"athelete"<<endl;
        }
};
class daughter : public parent{
    public:
        void profession(){ 
            cout<<"model"<<endl;
        }
};
int main(){
    son s;
    daughter d;
    cout<<"Son profession"<<endl;
    s.profession(); 
    cout<<"Daughter profession"<<endl;
    d.profession();
}
