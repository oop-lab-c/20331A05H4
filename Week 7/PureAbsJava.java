import java.util.*;
interface parent{  
    void career();
}

class son implements parent{  
    public void career(){
        System.out.println("Son choose sports as career");
    }
}

class daughter implements parent{  
    public void career(){
        System.out.println("Daughter choose acting as career");
    }
}

public class PureAbsJava {
    public static void main(String args[]){
        son s = new son();
        daughter d = new daughter();
        s.career();
        d.career();
    }
}

