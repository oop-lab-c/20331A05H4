import java.util.*;
class AccessspecifierDemo
{
    private int priVar;
    protected int proVar;
    public int pubVar;
    public void setVar(int priValue,int proValue,int pubValue)
    {
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    public void getVar()
    {
    System.out.println("Private VAriable : "+priVar);
    System.out.println("Protected Variable : "+proVar);
    System.out.println("Public Variable : "+pubVar);
    }
}
class Main
{
    public static void main(String[] args)
    {
        AccessspecifierDemo obj = new AccessspecifierDemo();
        obj.setVar(16,48,88);
        obj.getVar();
    }
}