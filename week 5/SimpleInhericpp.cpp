#include<iostream>
using namespace std;
class Details
{
  public:
  string name = "Manoj";
};
class Age : public Details
{
  public:
  int age = 19;
};
int main()
{
    Age obj;
    cout<<"Name : "<<obj.name<<endl;
    cout<<"Age : "<<obj.age<<endl;
    return 0;
}